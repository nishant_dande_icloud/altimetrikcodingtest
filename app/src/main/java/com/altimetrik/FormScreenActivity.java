package com.altimetrik;

import android.os.Bundle;
import android.support.annotation.Size;
import android.support.design.widget.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.altimetrik.utils.AppUtils;
import com.altimetrik.utils.Router;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nishantdande on 22/10/16.
 */

public class FormScreenActivity extends BaseActivity implements Validator.ValidationListener {

    @BindView(R.id.username)
    @NotEmpty
    @Length(min = 6, message = "Enter at least 6 characters.")
    TextInputEditText mUsername;

    @BindView(R.id.password)
    @NotEmpty
    @Length(min = 6, message = "Enter at least 6 characters.")
    TextInputEditText mPassword;

    @BindView(R.id.login)
    Button mLogin;

    private Validator validator;
    private boolean isUsernameValid;
    private boolean isPasswordValid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_screen);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);

        mUsername.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length()>6){
                    if (!AppUtils.isAlphaNumeric(getUsername())){
                        mUsername.setError(getString(R.string.invalid_username));
                    }
                    else{
                        isUsernameValid = true;
                        mUsername.setError(null);
                    }
                }else {
                    isUsernameValid = false;
                    mUsername.setError("Enter at least 6 characters.");
                }

                setLoginButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        mPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().length()>6){
                    isPasswordValid = true;
                    mPassword.setError(null);
                }else {
                    isPasswordValid = false;
                    mPassword.setError("Enter at least 6 characters.");
                }

                setLoginButton();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void setLoginButton() {
        if (isUsernameValid && isPasswordValid)
            mLogin.setEnabled(true);
        else
            mLogin.setEnabled(false);
    }

    @OnClick(R.id.login)
    public void login(){
        if (!AppUtils.isAlphaNumeric(getUsername())){
            mUsername.setError(getString(R.string.invalid_username));
            return;
        }

        if (!AppUtils.isAlphaNumeric(getPassword())){
            mPassword.setError(getString(R.string.invalid_password));
            return;
        }

        validator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        Bundle bundle = new Bundle();
        bundle.putString("username", mUsername.getText().toString());
        Router.startHomeScreenActivity(this, bundle);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                showError(message);
            }
        }
    }

    public String getUsername() {
        return mUsername.getText().toString();
    }

    public String getPassword() {
        return mPassword.getText().toString();
    }
}
