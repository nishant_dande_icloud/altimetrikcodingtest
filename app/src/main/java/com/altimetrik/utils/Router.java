package com.altimetrik.utils;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.altimetrik.HomeScreenActivity;

/**
 * Created by nishantdande on 22/10/16.
 */

public class Router {

    public static void startHomeScreenActivity(Activity activity, Bundle bundle){

        Intent intent = new Intent(activity, HomeScreenActivity.class);
        if (bundle!= null)
            intent.putExtras(bundle);

        activity.startActivity(intent);

    }
}
